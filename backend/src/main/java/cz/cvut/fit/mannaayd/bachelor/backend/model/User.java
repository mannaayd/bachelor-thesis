package cz.cvut.fit.mannaayd.bachelor.backend.model;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="appuser")
public class User{
    @Id
    @GeneratedValue
    private Long Id = 0L;

    @NonNull
    @Column(name="username", nullable = false, unique = true)
    private String username;
    @Column(name="password", nullable = false)
    private String password;
    @Column(name="email", nullable = false, unique = true)
    private String email;

    @Column(name="rank", nullable = false)
    private int rank;

    @Column(name="region", nullable = false)
    private String region;
    @Column(name="city", nullable = false)
    private String city;
    @Column(name="user_role", nullable = false)
    private String role;

    @ManyToMany(mappedBy = "respondents", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Question> questions;

    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    private List<Survey> surveys;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Answer> answers;

    public User(String username, String password, String email, int rank, String region, String city, String role, List<Question> questions, List<Survey> surveys, List<Answer> answers) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.rank = rank;
        this.region = region;
        this.city = city;
        this.role = role;
        this.questions = questions;
        this.surveys = surveys;
        this.answers = answers;
    }

    public User() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
