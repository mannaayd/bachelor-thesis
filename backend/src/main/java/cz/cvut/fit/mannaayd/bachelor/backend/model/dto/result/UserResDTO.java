package cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result;

public class UserResDTO {
    public String username;
    public String email;
    public int rank;
    public String region;
    public String city;

    public UserResDTO(String username, String email, int rank, String region, String city) {
        this.username = username;
        this.email = email;
        this.rank = rank;
        this.region = region;
        this.city = city;
    }
}
