package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

public class LoginFormDTO {
    public String username;
    public String password;

    public LoginFormDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginFormDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
