package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {
    public Long Id;
    public String username;
    public String email;
    public String password;
    public int rank;
    public String region;
    public String city;
    private String role;

    private List<Long> questionIds;
    private List<Long> surveyIds;
    private List<Long> answerIds;

    public UserDTO(Long id, String username, String email, String role,  int rank, String region, String city, List<Long> questionIds, List<Long> surveyIds, List<Long> answerIds) {
        this.Id = id;
        this.username = username;
        this.email = email;
        this.rank = rank;
        this.region = region;
        this.city = city;
        this.role = role;
        this.questionIds = questionIds;
        this.surveyIds = surveyIds;
        this.answerIds = answerIds;
        if(this.answerIds == null) this.answerIds = new ArrayList<Long>();
        if(this.surveyIds == null) this.surveyIds = new ArrayList<Long>();
        if(this.questionIds == null) this.questionIds = new ArrayList<Long>();
    }

    public UserDTO() {
        Id = 0L;
        role = "USER";
        answerIds = new ArrayList<Long>();
        surveyIds = new ArrayList<Long>();
        questionIds = new ArrayList<Long>();
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRole() {
        return role;
    }

    public List<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Long> questionIds) {
        this.questionIds = questionIds;
    }

    public List<Long> getSurveyIds() {
        return surveyIds;
    }

    public void setSurveyIds(List<Long> surveyIds) {
        this.surveyIds = surveyIds;
    }

    public List<Long> getAnswerIds() {
        return answerIds;
    }

    public void setAnswerIds(List<Long> answerIds) {
        this.answerIds = answerIds;
    }
}
