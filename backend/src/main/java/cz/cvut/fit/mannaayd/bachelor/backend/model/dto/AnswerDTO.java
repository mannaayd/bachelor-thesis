package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;

public class AnswerDTO {
    public Long Id;
    public int answer110 = 1;
    public String answer;
    public Long questionId;
    public Long userId;

    public AnswerDTO(Long id, int answer110, String answer, Long questionId, Long userId) {
        Id = id;
        this.answer110 = answer110;
        this.answer = answer;
        this.questionId = questionId;
        this.userId = userId;
    }

    public AnswerDTO(){}

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getAnswer110() {
        return answer110;
    }

    public void setAnswer110(int answer110) {
        this.answer110 = answer110;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
