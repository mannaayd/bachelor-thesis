package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.SurveyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Survey service interface for operations with Survey entity
 */
public interface ISurveyService {
    List<Survey> getAll();
    Optional<Survey> getSurveyById(Long surveyId);
    Survey save(Survey survey);
    Survey save(SurveyDTO surveyDTO)  throws NotExistsException;
    boolean deleteSurveyById(Long surveyId) throws NotExistsException;
    List<Survey> getSurveysByUserId(Long userId) throws NotExistsException;
    User getSurveyAuthor(Long surveyId) throws NotExistsException;
    News getSurveyNews(Long surveyId) throws NotExistsException;
    List<Question> getSurveyQuestions(Long surveyId) throws NotExistsException;
}
