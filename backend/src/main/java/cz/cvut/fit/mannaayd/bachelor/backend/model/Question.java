package cz.cvut.fit.mannaayd.bachelor.backend.model;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue
    private Long Id = 0L;

    @NonNull
    private String description;

    @ManyToOne
    @NonNull
    @JoinColumn(nullable = false)
    private Survey survey;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "question_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "question_id")
    )
    private List<User> respondents;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    @NonNull
    private List<Answer> answers;

    public Question() {
        description = "";
    }

    public Question(@NonNull String description, @NonNull Survey survey, @NonNull List<User> respondents, @NonNull List<Answer> answers) {
        this.description = description;
        this.survey = survey;
        this.respondents = respondents;
        this.answers = answers;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    @NonNull
    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(@NonNull Survey survey) {
        this.survey = survey;
    }

    @NonNull
    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(@NonNull List<Answer> answers) {
        this.answers = answers;
    }

    @NonNull
    public List<User> getRespondents() {
        return respondents;
    }

    public void setRespondents(@NonNull List<User> respondents) {
        this.respondents = respondents;
    }
}
