package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.NewsMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.QuestionMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.QuestionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * Question controller that handles operations with Question entity
 */
@RestController
@CrossOrigin
public class QuestionController implements IController<QuestionDTO> {
    final private QuestionMapper questionMapper;
    final private QuestionService questionService;
    final private NewsMapper newsMapper;

    public QuestionController(QuestionMapper questionMapper, QuestionService questionService, NewsMapper newsMapper) {
        this.questionMapper = questionMapper;
        this.questionService = questionService;
        this.newsMapper = newsMapper;
    }

    /**
     * Gets all questions
     * @return list of questions
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/questions")
    public ResponseEntity<List<QuestionDTO>> all() {
        try {
            List<Question> questions = questionService.getAll();
            return new ResponseEntity<>(questionMapper.toListDTO(questions), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get question by id
     * @param id
     * @return question by id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/questions/{id}")
    public ResponseEntity<QuestionDTO> byID(@PathVariable Long id) {
        try {
            Optional<Question> question = questionService.getQuestionById(id);
            if(question.isEmpty()) throw new NotExistsException("Question");
            return new ResponseEntity<>(questionMapper.toDTO(question.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets question tweet
     * @param id
     * @return news entity with tweet id
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/questions/{id}/tweet")
    public ResponseEntity<NewsDTO> tweetById(@PathVariable Long id) {
        try {
            Optional<Question> question = questionService.getQuestionById(id);
            if(question.isEmpty()) throw new NotExistsException("Question");
            return new ResponseEntity<>(newsMapper.toDTO(question.get().getSurvey().getNews()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update question by id
     * @param id
     * @param questionDTO
     * @return updated question
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/questions/{id}")
    public ResponseEntity<QuestionDTO> updateById(@PathVariable Long id, @RequestBody QuestionDTO questionDTO) {
        try {
            Optional<Question> question = questionService.getQuestionById(id);
            if(question.isEmpty()) throw new NotExistsException("Question");
            Question updatedQuestion = questionMapper.toEntity(questionDTO);
            updatedQuestion.setId(id);
            questionService.save(updatedQuestion);
            return new ResponseEntity<>(questionDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create question
     * @param questionDTO
     * @return created question
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/questions")
    public ResponseEntity<QuestionDTO> create(@RequestBody QuestionDTO questionDTO) {
        try {
            questionService.save(questionDTO);
            return new ResponseEntity<>(questionDTO, HttpStatus.OK);
        }
        catch (NotExistsException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        catch (BadRequestException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete question from db
     * @param id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/questions/{id}")
    public void delete(@PathVariable Long id) {
        questionService.deleteQuestionById(id);
    }
}
