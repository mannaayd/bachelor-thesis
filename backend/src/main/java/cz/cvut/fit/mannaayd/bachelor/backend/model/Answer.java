package cz.cvut.fit.mannaayd.bachelor.backend.model;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="answer")
public class Answer {
    @Id
    @GeneratedValue
    private Long Id = 0L;
    @Column(name = "answer110", nullable = false)
    int answer110;
    @Column(name = "answer_description", nullable = true)
    String answer;
    @ManyToOne
    @NonNull
    @JoinColumn(nullable=false)
    private Question question;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    public Answer(){
        answer = "";
    }

    public Answer(int answer110, String answer, @NonNull Question question, User user) {
        this.answer110 = answer110;
        this.answer = answer;
        this.question = question;
        this.user = user;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getAnswer110() {
        return answer110;
    }

    public void setAnswer110(int answer110) {
        this.answer110 = answer110;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @NonNull
    public Question getQuestion() {
        return question;
    }

    public void setQuestion(@NonNull Question question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
