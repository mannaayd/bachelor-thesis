package cz.cvut.fit.mannaayd.bachelor.backend.model;

import cz.cvut.fit.mannaayd.bachelor.backend.model.authorities.AdminAuthority;
import cz.cvut.fit.mannaayd.bachelor.backend.model.authorities.ManagerAuthority;
import cz.cvut.fit.mannaayd.bachelor.backend.model.authorities.UserAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyUserPrincipal implements UserDetails {
    private User user;

    public MyUserPrincipal(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if (user.getRole().equals("ADMIN")) authorities.add(new AdminAuthority());
        else if(user.getRole().equals("MANAGER")) authorities.add(new ManagerAuthority());
        else if(user.getRole().equals("USER")) authorities.add(new UserAuthority());
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
