package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.QuestionMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.AnswerRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.QuestionRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionService implements IQuestionService {
    public QuestionMapper questionMapper;
    public QuestionRepository questionRepository;
    public UserRepository userRepository;
    public AnswerService answerService;

    public QuestionService(QuestionMapper questionMapper, QuestionRepository questionRepository, UserRepository userRepository, AnswerService answerService) {
        this.questionMapper = questionMapper;
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.answerService = answerService;
    }

    @Override
    public List<Question> getAll() {
        return questionRepository.findAll();
    }

    @Override
    public Optional<Question> getQuestionById(Long questionId) {
        return questionRepository.findById(questionId);
    }

    @Override
    public Question save(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public Question save(QuestionDTO questionDTO) throws NotExistsException, BadRequestException {
        return questionRepository.save(questionMapper.toEntity(questionDTO));
    }

    @Override
    public boolean deleteQuestionById(Long questionId) {
        Optional<Question> question = questionRepository.findById(questionId);
        if(question.isEmpty()) return false;
        questionRepository.delete(question.get());
        return true;
    }

    @Override
    public List<Question> getQuestionsByUserId(Long userId) throws NotExistsException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) throw new NotExistsException("User");
        return user.get().getQuestions();
    }

    @Override
    public List<Question> getAnsweredQuestionsByUserId(Long userId) throws NotExistsException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) throw new NotExistsException("User");
        List<Question> answered = new ArrayList<Question>();
        List<Answer> answers = answerService.getAnswersByUserId(userId);
        List<Question> questions = getQuestionsByUserId(userId);
        for (Question question: questions) {
            if(question.getSurvey().isEnded()){
                answered.add(question);
                continue;
            }
            for (Answer answer: answers) {
                if(answer.getQuestion().getId().equals(question.getId())){
                    answered.add(question);
                    break;
                }
            }
        }

        return answered;
    }

    @Override
    public List<Question> getUnansweredQuestionsByUserId(Long userId) throws NotExistsException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) throw new NotExistsException("User");
        List<Question> answered = new ArrayList<Question>();
        List<Answer> answers = answerService.getAnswersByUserId(userId);
        List<Question> questions = getQuestionsByUserId(userId);
        for (Question question: questions) {
            boolean flag = true;
            if(question.getSurvey().isEnded()) flag = false;
            for (Answer answer: answers) {
                if(answer.getQuestion().getId().equals(question.getId())){
                    flag = false;
                    break;
                }
            }
            if(flag) answered.add(question);
        }
        return answered;
    }

    @Override
    public Question addUsersFromRegion(Long questionId, String region) throws NotExistsException {
        Optional<Question> question = questionRepository.findById(questionId);
        if(question.isEmpty()) throw new NotExistsException("Question");

        return null;
    }

    @Override
    public Question addUsersFromRegionAndCity(Long questionId, String region, String city) throws NotExistsException {
        return null;
    }
}
