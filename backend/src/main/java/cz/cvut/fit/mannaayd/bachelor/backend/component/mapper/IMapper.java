package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;

import java.util.LinkedList;
import java.util.List;

/**
 * Maps ENTITY to DTO and DTO to ENTITY, also can encode list of ENTITY into list of DTO
 * @param <ENTITY> entity class to map from DTO
 * @param <DTO> DTO class to map from ENTITY
 */
public interface IMapper<ENTITY, DTO> {
    /**
     * Maps from DTO to ENTITY method declaration
     * @param dto
     * @return ENTITY
     * @throws NotExistsException when some of relation properties does not exist
     * @throws BadRequestException when something goes wrong
     */
    ENTITY toEntity(DTO dto) throws NotExistsException, BadRequestException;

    /**
     * Maps from ENTITY to DTO method declaration
     * @param entity
     * @return DTO
     */
    DTO toDTO(ENTITY entity);

    /**
     * Maps from list of ENTITY to list of DTO, default method, all classes that implements this interface will get implementation of this method
     * @param entities
     * @return list of DTO objects
     */
    default List<DTO> toListDTO(List<ENTITY> entities)
    {
        List<DTO> DTOs = new LinkedList<DTO>();
        for (ENTITY entity: entities) {
            DTOs.add(toDTO(entity));
        }
        return DTOs;
    }
}
