package cz.cvut.fit.mannaayd.bachelor.backend.exception;

/**
 * Throws in most error cases
 */
public class BadRequestException extends Exception{
    public String message;
    public BadRequestException(String cause) {
        message = "Message: " + cause;
    }
}
