package cz.cvut.fit.mannaayd.bachelor.backend;

import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class BackendNewsVerificationAppApplication {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Entrypoint of whole backend
	 * @param args arguments to pass by
	 */
	public static void main(String[] args) {
		SpringApplication.run(BackendNewsVerificationAppApplication.class, args);
	}

	/**
	 * Initializer of database
	 * @return lambda with init func
	 */
	@Bean
	InitializingBean sendDatabase() {
		return () -> {
			if(userRepository.findByUsername("admin").isPresent()) return;
			User admin = new User();
			admin.setUsername("admin");
			admin.setPassword(passwordEncoder.encode("12345678"));
			admin.setRank(5);
			admin.setRole("ADMIN");
			admin.setEmail("admin@admins.cz");
			admin.setCity("Prague");
			admin.setRegion("EU");
			userRepository.save(admin);
			User manager = new User();
			manager.setUsername("manager");
			manager.setPassword(passwordEncoder.encode("12345678"));
			manager.setRank(5);
			manager.setRole("MANAGER");
			manager.setEmail("manager@managers.cz");
			manager.setCity("Prague");
			manager.setRegion("EU");
			userRepository.save(manager);

		};
	}
}

