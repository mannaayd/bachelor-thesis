package cz.cvut.fit.mannaayd.bachelor.backend.repository;

import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import java.util.List;
import java.util.Optional;

/**
 * Question repository to communicate with database via Hibernate
 */
public interface QuestionRepository extends JpaRepository<Question, Long> {
    @Override
    Optional<Question> findById(Long id);

    @Override
    List<Question> findAll();

}
