package cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result;

import java.util.List;

public class ResultDTO{
    private String tweetId;
    private String city;
    private String region;
    private List<QuestionResDTO> questions;


    public ResultDTO(String tweetId, String city, String region, List<QuestionResDTO> questions) {
        this.tweetId = tweetId;
        this.city = city;
        this.region = region;
        this.questions = questions;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public List<QuestionResDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionResDTO> questions) {
        this.questions = questions;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
