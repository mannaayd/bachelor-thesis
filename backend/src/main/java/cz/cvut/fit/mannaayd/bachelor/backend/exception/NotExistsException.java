package cz.cvut.fit.mannaayd.bachelor.backend.exception;

/**
 * Throws when something didnt exist in db
 */
public class NotExistsException extends Exception{
    public String message;
    public NotExistsException(String entity) {
        message = entity + " is not exists in database";
    }
}
