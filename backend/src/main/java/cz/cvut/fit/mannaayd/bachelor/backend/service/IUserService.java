package cz.cvut.fit.mannaayd.bachelor.backend.service;


import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.UserDTO;

import java.util.List;
import java.util.Optional;

/**
 * User service interface for operations with User entity
 */
public interface IUserService {
    List<User> getAll();
    Optional<User> getUserById(Long userId);
    User save(User user);
    User save(UserDTO userDTO)  throws NotExistsException;
    boolean deleteUserById(Long userId) throws NotExistsException;
    Optional<User> getUserByUsername(String username);
}
