package cz.cvut.fit.mannaayd.bachelor.backend.model;

public class TwitterCredentials {
    public String apiKey;
    public String apiSecretKey;
    public String accessToken;
    public String accessTokenSecret;
    public String bearerToken;

    public TwitterCredentials(String apiKey, String apiSecretKey, String accessToken, String accessTokenSecret) {
        this.apiKey = apiKey;
        this.apiSecretKey = apiSecretKey;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
    }

    public TwitterCredentials() {
    }
}
