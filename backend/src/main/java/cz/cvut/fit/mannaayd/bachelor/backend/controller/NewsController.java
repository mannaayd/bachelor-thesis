package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.TweetListDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.TwitterService;
import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.NewsMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.NewsService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * News controller that handles operations with News entity
 */
@RestController
@CrossOrigin
public class NewsController implements IController<NewsDTO> {
    final private NewsService newsService;
    final private NewsMapper newsMapper;
    final private TwitterService twitterService;

    public NewsController(NewsService newsService, NewsMapper newsMapper, TwitterService twitterService) {
        this.newsService = newsService;
        this.newsMapper = newsMapper;
        this.twitterService = twitterService;
    }


    /**
     * Gets all news from db
     * @return list of news
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/news")
    public ResponseEntity<List<NewsDTO>> all() {
        try {
            List<News> news = newsService.getAll();
            return new ResponseEntity<>(newsMapper.toListDTO(news), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get news by id
     * @param id
     * @return news with id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/news/{id}")
    public ResponseEntity<NewsDTO> byID(@PathVariable Long id) {
        try {
            Optional<News> news = newsService.getNewsById(id);
            if(news.isEmpty()) throw new NotExistsException("News");
            return new ResponseEntity<>(newsMapper.toDTO(news.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update news with id
     * @param id
     * @param newsDTO
     * @return updated news
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/news/{id}")
    public ResponseEntity<NewsDTO> updateById(@PathVariable Long id, @RequestBody NewsDTO newsDTO) {
        try {
            Optional<News> news = newsService.getNewsById(id);
            if(news.isEmpty()) throw new NotExistsException("News");
           News updatedNews = newsMapper.toEntity(newsDTO);
            updatedNews.setId(id);
            newsService.save(updatedNews);
            return new ResponseEntity<>(newsDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create new news
     * @param newsDTO
     * @return created news
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/news")
    public ResponseEntity<NewsDTO> create(@RequestBody NewsDTO newsDTO) {
        try {
            newsDTO.setId(newsService.save(newsDTO).getId());
            return new ResponseEntity<>(newsDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete news from database
     * @param id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/news/{id}")
    public void delete(@PathVariable Long id) {
        newsService.deleteNewsById(id);
    }

    /**
     * Gets last N news from twitter
     * @param count
     * @return list of relevant news
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/news/last/{count}")
    public ResponseEntity<TweetListDTO> last(@PathVariable int count) {
        try {
            return new ResponseEntity<>(twitterService.getLastTweets(count), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }
}
