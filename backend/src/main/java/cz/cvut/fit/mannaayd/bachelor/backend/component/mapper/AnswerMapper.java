package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.QuestionRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Maps Answer to AnswerDTO and AnswerDTO to Answer, also can encode list of Answers into list of AnswerDTO
 */
@Component
public class AnswerMapper implements IMapper<Answer, AnswerDTO> {
    public UserRepository userRepository;
    public QuestionRepository questionRepository;

    @Autowired
    public AnswerMapper(UserRepository _userRepository, QuestionRepository _questionRepository) {
        userRepository = _userRepository;
        questionRepository = _questionRepository;
    }

    @Override
    public Answer toEntity(AnswerDTO answerDTO) throws NotExistsException, BadRequestException {
        Optional<User> user = userRepository.findById(answerDTO.getUserId());
        Optional<Question> question = questionRepository.findById(answerDTO.getQuestionId());
        if(user.isEmpty()) throw new NotExistsException("User ");
        if(question.isEmpty()) throw new NotExistsException("Question ");
        if(answerDTO.answer110 > 10 || answerDTO.answer110 < 1) throw new BadRequestException("Answer must be an integer between 1-10");
        for(Answer answer : user.get().getAnswers()){
            if(answer.getQuestion().getId().equals(question.get().getId())) throw new BadRequestException("User already answer for this question.");
        }

        return new Answer(answerDTO.getAnswer110(), answerDTO.getAnswer(), question.get(), user.get());
    }


    @Override
    public AnswerDTO toDTO(Answer answer) {
        return new AnswerDTO(answer.getId(), answer.getAnswer110(), answer.getAnswer(), answer.getQuestion().getId(), answer.getUser().getId());
    }
}
