package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.SurveyMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.*;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.SurveyDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.SurveyRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SurveyService implements ISurveyService {
    public SurveyMapper surveyMapper;
    public SurveyRepository surveyRepository;
    public UserRepository userRepository;

    public SurveyService(SurveyMapper surveyMapper, SurveyRepository surveyRepository, UserRepository userRepository) {
        this.surveyMapper = surveyMapper;
        this.surveyRepository = surveyRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Survey> getAll() {
        return surveyRepository.findAll();
    }

    @Override
    public Optional<Survey> getSurveyById(Long surveyId) {
        return surveyRepository.findById(surveyId);
    }

    @Override
    public Survey save(Survey survey) {
        return surveyRepository.save(survey);
    }

    @Override
    public Survey save(SurveyDTO surveyDTO) throws NotExistsException {
        return surveyRepository.save(surveyMapper.toEntity(surveyDTO));
    }

    @Override
    public boolean deleteSurveyById(Long surveyId) {
        Optional<Survey> survey = surveyRepository.findById(surveyId);
        if(survey.isEmpty()) return false;
        surveyRepository.delete(survey.get());
        return true;
    }

    @Override
    public List<Survey> getSurveysByUserId(Long userId) throws NotExistsException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) throw new NotExistsException("User");
        return user.get().getSurveys();
    }

    @Override
    public User getSurveyAuthor(Long surveyId) throws NotExistsException {
        Optional<Survey> survey = getSurveyById(surveyId);
        if(survey.isEmpty()) throw new NotExistsException("Survey");
        return survey.get().getAuthor();
    }

    @Override
    public News getSurveyNews(Long surveyId) throws NotExistsException {
        Optional<Survey> survey = getSurveyById(surveyId);
        if(survey.isEmpty()) throw new NotExistsException("Survey");
        return survey.get().getNews();
    }

    @Override
    public List<Question> getSurveyQuestions(Long surveyId) throws NotExistsException {
        Optional<Survey> survey = getSurveyById(surveyId);
        if(survey.isEmpty()) throw new NotExistsException("Survey");
        return survey.get().getQuestions();
    }
}
