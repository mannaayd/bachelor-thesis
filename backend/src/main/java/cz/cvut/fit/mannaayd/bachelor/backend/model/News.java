package cz.cvut.fit.mannaayd.bachelor.backend.model;

import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue
    private Long Id = 0L;

    public News(){}

    public News(@NonNull String tweetId, @NonNull String type, @NonNull String city, @NonNull String region, Survey survey) {
        this.tweetId = tweetId;
        this.type = type;
        this.city = city;
        this.region = region;
        this.survey = survey;
    }

    @NonNull
    @Column(unique = true)
    private String tweetId;
    @NonNull
    private String type;

    @NonNull
    private String city;
    @NonNull
    private String region;

    @OneToOne
    @JoinColumn(name = "survey", nullable = true)
    private Survey survey;

    public Survey getSurvey() {
        return survey;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    @NonNull
    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(@NonNull String tweetId) {
        this.tweetId = tweetId;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    @NonNull
    public String getCity() {
        return city;
    }

    public void setCity(@NonNull String city) {
        this.city = city;
    }

    @NonNull
    public String getRegion() {
        return region;
    }

    public void setRegion(@NonNull String region) {
        this.region = region;
    }
}
