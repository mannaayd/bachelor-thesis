package cz.cvut.fit.mannaayd.bachelor.backend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.model.TwitterCredentials;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.TweetDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.TweetListDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Twitter Service that gets data from Twitter API
 */
@Service
public class TwitterService {
    final private String apiUrl = "https://api.twitter.com/2/";
    final private List<String> sources;

    final private RestTemplate restTemplate;
    private TwitterCredentials twitterCredentials;

    /**
     * Twitter Service constructor
     * @param restTemplateBuilder basic http client for requests
     */
    public TwitterService(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.build();
        this.sources = new ArrayList<String>();
        sources.add("ceskezpravy_24"); // adding news sources
        sources.add("BBCBreaking");

        try {
            ObjectMapper objectMapper = new ObjectMapper(); // get keys from file
            this.twitterCredentials = objectMapper.readValue(new File("twitter_keys.json"), TwitterCredentials.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get last relevant news from twitter
     * @param count of tweets
     * @return list of tweets from news sources
     * @throws Exception when something went wrong with http client
     */
    public TweetListDTO getLastTweets(int count) throws Exception {
        if(count < 0) throw new Exception("bad count");
        String lastTweetsUrl = "tweets/search/recent";

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(twitterCredentials.bearerToken); // add bearer token(JWT) into request header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> request = new HttpEntity<>(headers);
        List<TweetDTO> tweetDTOs = new ArrayList<TweetDTO>();
        for(String source : sources) {
            String query = "?query=from:" + source +"&max_results=" + Integer.toString(count/sources.size());
            String url = apiUrl + lastTweetsUrl + query;
            ResponseEntity<TweetListDTO> response = this.restTemplate.exchange(url, HttpMethod.GET, request, TweetListDTO.class, 1);
            tweetDTOs.addAll(response.getBody().getData());
        }
        TweetListDTO res = new TweetListDTO();
        res.setData(tweetDTOs);
        return res;
    }
}
