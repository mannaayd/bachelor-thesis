package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import java.util.ArrayList;
import java.util.List;

public class TweetListDTO {
    private List<TweetDTO> data;


    public TweetListDTO(List<TweetDTO> data) {
        this.data = data;
    }

    public TweetListDTO() {
        data = new ArrayList<TweetDTO>();
    }

    public List<TweetDTO> getData() {
        return data;
    }

    public void setData(List<TweetDTO> data) {
        this.data = data;
    }
}
