package cz.cvut.fit.mannaayd.bachelor.backend.repository;

import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * News repository to communicate with database via Hibernate
 */
public interface NewsRepository extends JpaRepository<News, Long> {
    @Override
    Optional<News> findById(Long id);

    @Override
    List<News> findAll();

}
