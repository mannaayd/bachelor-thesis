package cz.cvut.fit.mannaayd.bachelor.backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
/**
 * JWT config class that have some security configs
 */
public class JwtConfig {

    @Value("${app.security.jwt.keystore-location}")
    private String keyStorePath; // keystore from local disk lo sign tokens

    @Value("${app.security.jwt.keystore-password}")
    private String keyStorePassword; // keystore password

    @Value("${app.security.jwt.key-alias}")
    private String keyAlias;

    @Value("${app.security.jwt.private-key-passphrase}")
    private String privateKeyPassphrase;

    @Bean
    /**
     * Opens keystore get necessary keys
     * @return openedkeystore
     */
    public KeyStore keyStore() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(keyStorePath);
            keyStore.load(resourceAsStream, keyStorePassword.toCharArray());
            return keyStore;
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            System.out.println("Unable to load keystore: {}");
        }

        throw new IllegalArgumentException("Unable to load keystore");
    }

    @Bean
    /**
     * Gets RSA private key for signing from keystore
     * @param KeyStore keyStore keystore with all keys
     * @return RSA private key
     */
    public RSAPrivateKey jwtSigningKey(KeyStore keyStore) {
        try {
            Key key = keyStore.getKey(keyAlias, privateKeyPassphrase.toCharArray());
            if (key instanceof RSAPrivateKey) {
                return (RSAPrivateKey) key;
            }
        } catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
            System.out.println("Unable to load private key from keystore: {}");
        }

        throw new IllegalArgumentException("Unable to load private key");
    }

    @Bean
    /**
     * Gets RSA public key for signing from keystore
     * @param KeyStore keyStore keystore with all keys
     * @return RSA public key
     */
    public RSAPublicKey jwtValidationKey(KeyStore keyStore) {
        try {
            Certificate certificate = keyStore.getCertificate(keyAlias);
            PublicKey publicKey = certificate.getPublicKey();

            if (publicKey instanceof RSAPublicKey) {
                return (RSAPublicKey) publicKey;
            }
        } catch (KeyStoreException e) {
            System.out.println("Unable to load private key from keystore: {}");
        }

        throw new IllegalArgumentException("Unable to load RSA public key");
    }

    @Bean
    /**
     * Builds JWT decoder jwt to verify it
     * @param RSAPublicKey rsaPublicKey public RSA key
     * @return JwtDecoder class
     */
    public JwtDecoder jwtDecoder(RSAPublicKey rsaPublicKey) {
        return NimbusJwtDecoder.withPublicKey(rsaPublicKey).build();
    }
}
