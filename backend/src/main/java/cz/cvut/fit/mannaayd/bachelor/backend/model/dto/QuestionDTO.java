package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import java.util.List;

public class QuestionDTO {
    public Long Id;
    public String description;
    public Long surveyId;
    public List<Long> respondentIds;
    public List<Long> answerIds;


    public QuestionDTO(Long id, String description, Long surveyId, List<Long> respondents, List<Long> answerIds) {
        Id = id;
        this.description = description;
        this.surveyId = surveyId;
        this.respondentIds = respondents;
        this.answerIds = answerIds;
    }

    public QuestionDTO() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public List<Long> getRespondentIds() {
        return respondentIds;
    }

    public void setRespondentIds(List<Long> respondentIds) {
        this.respondentIds = respondentIds;
    }

    public List<Long> getAnswerIds() {
        return answerIds;
    }

    public void setAnswerIds(List<Long> answerIds) {
        this.answerIds = answerIds;
    }
}
