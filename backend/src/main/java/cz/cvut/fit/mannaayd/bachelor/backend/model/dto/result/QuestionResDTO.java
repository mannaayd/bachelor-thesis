package cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result;

import java.util.List;

public class QuestionResDTO {
    public String question;
    public List<AnswerResDTO> answers;


    public QuestionResDTO(String question, List<AnswerResDTO> answers) {
        this.question = question;
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<AnswerResDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerResDTO> answers) {
        this.answers = answers;
    }
}
