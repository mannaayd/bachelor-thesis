package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

public class NewsDTO {
    public Long Id;
    public String tweetId;
    public String type;
    public String city;
    public String region;
    public Long surveyId = null;

    public NewsDTO(Long id, String tweetId, String type, String city, String region, Long surveyId) {
        Id = id;
        this.tweetId = tweetId;
        this.type = type;
        this.city = city;
        this.region = region;
        this.surveyId = surveyId;
    }

    // ???
    public NewsDTO(Long id, String tweetId, String type, String city, String region) {
        Id = id;
        this.tweetId = tweetId;
        this.type = type;
        this.city = city;
        this.region = region;
    }

    public NewsDTO() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }
}
