package cz.cvut.fit.mannaayd.bachelor.backend.repository;

import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Answer repository to communicate with database via Hibernate
 */
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    @Override
    Optional<Answer> findById(Long id);

    @Override
    List<Answer> findAll();

}
