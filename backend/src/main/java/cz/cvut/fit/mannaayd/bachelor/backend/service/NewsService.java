package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.NewsMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.NewsRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NewsService implements INewsService {
    public NewsMapper newsMapper;
    public NewsRepository newsRepository;
    public UserRepository userRepository;

    public NewsService(NewsMapper newsMapper, NewsRepository newsRepository, UserRepository userRepository) {
        this.newsMapper = newsMapper;
        this.newsRepository = newsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<News> getAll() {
        return newsRepository.findAll();
    }

    @Override
    public Optional<News> getNewsById(Long newsId) {
        return newsRepository.findById(newsId);
    }

    @Override
    public News save(News news) {
        return newsRepository.save(news);
    }

    @Override
    public News save(NewsDTO newsDTO) throws NotExistsException {
        return newsRepository.save(newsMapper.toEntity(newsDTO));
    }

    @Override
    public boolean deleteNewsById(Long newsId) {
        Optional<News> news = newsRepository.findById(newsId);
        if(news.isEmpty()) return false;
        newsRepository.delete(news.get());
        return true;
    }
}
