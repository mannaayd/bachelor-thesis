package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.*;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.SurveyDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.UserDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result.ResultDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.SurveyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * Survey controller that handles operations with Survey entity
 */
@RestController
@CrossOrigin
public class SurveyController implements IController<SurveyDTO> {
    private final SurveyMapper surveyMapper;
    private final SurveyService surveyService;
    private final UserMapper userMapper;
    private final QuestionMapper questionMapper;
    private final NewsMapper newsMapper;
    private final ResultMapper resultMapper;

    public SurveyController(SurveyMapper surveyMapper, SurveyService surveyService, UserMapper userMapper, QuestionMapper questionMapper, NewsMapper newsMapper, ResultMapper resultMapper) {
        this.surveyMapper = surveyMapper;
        this.surveyService = surveyService;
        this.userMapper = userMapper;
        this.questionMapper = questionMapper;
        this.newsMapper = newsMapper;
        this.resultMapper = resultMapper;
    }

    /**
     * Get all surveys from db
     * @return list of surveys
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/surveys")
    public ResponseEntity<List<SurveyDTO>> all() {
        try {
            List<Survey> surveys = surveyService.getAll();
            return new ResponseEntity<>(surveyMapper.toListDTO(surveys), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get Survey by id
     * @param id
     * @return survey with id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/surveys/{id}")
    public ResponseEntity<SurveyDTO> byID(@PathVariable Long id) {
        try {
            Optional<Survey> survey = surveyService.getSurveyById(id);
            if(survey.isEmpty()) throw new NotExistsException("Survey");
            return new ResponseEntity<>(surveyMapper.toDTO(survey.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update Survey by id
     * @param id
     * @param surveyDTO
     * @return updated survey
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/surveys/{id}")
    public ResponseEntity<SurveyDTO> updateById(@PathVariable Long id, @RequestBody SurveyDTO surveyDTO) {
        try {
            Optional<Survey> survey = surveyService.getSurveyById(id);
            if(survey.isEmpty()) throw new NotExistsException("Survey");
            Survey updatedSurvey = surveyMapper.toEntity(surveyDTO);
            updatedSurvey.setId(id);
            surveyService.save(updatedSurvey);
            return new ResponseEntity<>(surveyDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Create survey
     * @param surveyDTO
     * @return created survey
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/surveys")
    public ResponseEntity<SurveyDTO> create(@RequestBody SurveyDTO surveyDTO) {
        try {
            surveyService.save(surveyDTO);
            return new ResponseEntity<>(surveyDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete survey from db
     * @param id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/surveys/{id}")
    public void delete(@PathVariable Long id) {
        surveyService.deleteSurveyById(id);
    }

    /**
     * Get survey author
     * @param id
     * @return survey author
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/surveys/{id}/author")
    public ResponseEntity<UserDTO> getAuthor(@PathVariable Long id){
        try {
            User user = surveyService.getSurveyAuthor(id);
            return new ResponseEntity<>(userMapper.toDTO(user), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get survey questions
     * @param id
     * @return list of survey questions
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/surveys/{id}/questions")
    public ResponseEntity<List<QuestionDTO>> getQuestions(@PathVariable Long id){
        try {
            List<Question> questions = surveyService.getSurveyQuestions(id);
            return new ResponseEntity<>(questionMapper.toListDTO(questions), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get survey news
     * @param id
     * @return survey news
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/surveys/{id}/news")
    public ResponseEntity<NewsDTO> getNews(@PathVariable Long id){
        try {
            News news = surveyService.getSurveyNews(id);
            return new ResponseEntity<>(newsMapper.toDTO(news), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get survey results
     * @param id
     * @return survey results
     */
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/surveys/{id}/results")
    public ResponseEntity<ResultDTO> getResults(@PathVariable Long id){
        try {
            Optional<Survey> survey = surveyService.getSurveyById(id);
            if(survey.isEmpty()) throw new NotExistsException("Survey");
            return new ResponseEntity<>(resultMapper.getResult(survey.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
