package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.AnswerMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.QuestionMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.SurveyMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.UserMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.SurveyDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.UserDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.AnswerService;
import cz.cvut.fit.mannaayd.bachelor.backend.service.QuestionService;
import cz.cvut.fit.mannaayd.bachelor.backend.service.SurveyService;
import cz.cvut.fit.mannaayd.bachelor.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * User controller that handles operations with User entity
 */
@RestController
@CrossOrigin
public class UserController implements IController<UserDTO> {
    final private UserMapper userMapper;
    final private UserService userService;
    final private AnswerService answerService;
    final private AnswerMapper answerMapper;
    final private SurveyService surveyService;
    final private SurveyMapper surveyMapper;
    final private QuestionService questionService;
    final private QuestionMapper questionMapper;
    final private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserMapper userMapper, UserService userService, AnswerService answerService, SurveyService surveyService, QuestionService questionService, AnswerMapper answerMapper, SurveyMapper surveyMapper, QuestionMapper questionMapper, PasswordEncoder passwordEncoder) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.answerService = answerService;
        this.surveyService = surveyService;
        this.questionService = questionService;
        this.answerMapper = answerMapper;
        this.questionMapper = questionMapper;
        this.surveyMapper = surveyMapper;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Get all users
     * @return list of users
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> all() {
        try {
            List<User> users = userService.getAll();
            return new ResponseEntity<>(userMapper.toListDTO(users), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get user by id
     * @param id
     * @return user with id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/users/{id}")
    public ResponseEntity<UserDTO> byID(@PathVariable Long id) {
        try {
            Optional<User> user = userService.getUserById(id);
            if(user.isEmpty()) throw new NotExistsException("User");
            return new ResponseEntity<>(userMapper.toDTO(user.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update user in db
     * @param id
     * @param userDTO
     * @return updated user
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/users/{id}")
    public ResponseEntity<UserDTO> updateById(@PathVariable Long id, @RequestBody UserDTO userDTO) {
        try {
            Optional<User> user = userService.getUserById(id);
            if(user.isEmpty()) throw new NotExistsException("User");
            User updatedUser = userMapper.toEntity(userDTO);
            updatedUser.setId(id);
            userService.save(updatedUser);
            userDTO.setPassword("");
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Register new user
     * @param userDTO includes password
     * @return regitered user without password
     */
    @Override
    @PostMapping("/register")
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO) {
        try {
            String password = userDTO.getPassword();
            userDTO.setPassword(passwordEncoder.encode(password)); // hashing password
            userDTO.setRank(1);
            userService.save(userDTO);
            userDTO.setPassword("");
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete user from db
     * @param id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Long id) {
        userService.deleteUserById(id);
    }

    /**
     * Get user answers
     * @param id
     * @return list of user answers
     */
    @GetMapping("/users/{id}/answers")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<List<AnswerDTO>> userAnswers(@PathVariable Long id)
    {
        try {
            List<Answer> answers = answerService.getAnswersByUserId(id);
            return new ResponseEntity<>(answerMapper.toListDTO(answers), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get user surveys
     * @param id
     * @return user surveys
     */
    @GetMapping("/users/{id}/surveys")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<List<SurveyDTO>> userSurveys(@PathVariable Long id)
    {
        try {
            List<Survey> surveys = surveyService.getSurveysByUserId(id);
            return new ResponseEntity<>(surveyMapper.toListDTO(surveys), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get questions that attached to users
     * @param id
     * @return list of questions
     */
    @GetMapping("/users/{id}/questions")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<List<QuestionDTO>> userQuestions(@PathVariable Long id)
    {
        try {
            List<Question> questions = questionService.getQuestionsByUserId(id);
            return new ResponseEntity<>(questionMapper.toListDTO(questions), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Gets user answered questions
     * @param id
     * @return list of answered questions
     */
    @GetMapping("/users/{id}/questions/answered")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    public ResponseEntity<List<QuestionDTO>> userQuestionsAnswered(@PathVariable Long id)
    {
        try {
            List<Question> questions = questionService.getAnsweredQuestionsByUserId(id);
            return new ResponseEntity<>(questionMapper.toListDTO(questions), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Gets unanswered questions of user
     * @param id
     * @return list of unanswered questions
     */
    @GetMapping("/users/{id}/questions/unanswered")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    public ResponseEntity<List<QuestionDTO>> userQuestionsUnanswered(@PathVariable Long id)
    {
        try {
            List<Question> questions = questionService.getUnansweredQuestionsByUserId(id);
            return new ResponseEntity<>(questionMapper.toListDTO(questions), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }
}
