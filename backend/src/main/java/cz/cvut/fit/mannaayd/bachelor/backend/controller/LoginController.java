package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.component.helper.JwtHelper;
import cz.cvut.fit.mannaayd.bachelor.backend.config.WebSecurityConfig;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.JwtDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.LoginFormDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
/**
 * Login controller that handle login endpoint and catch user errors
 */
public class LoginController {

    private final JwtHelper jwtHelper;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    public LoginController(JwtHelper jwtHelper, UserDetailsService userDetailsService, PasswordEncoder passwordEncoder, UserService userService) {
        this.jwtHelper = jwtHelper;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @PostMapping("/login")
    /**
     * Login endpoint that handle user login
     * @param LoginFormDTO loginForm
     * @return ResponseEntity<JwtDTO> return JWT token with necessary info about user
     */
    public ResponseEntity<JwtDTO> login(@RequestBody LoginFormDTO loginForm) {

        System.out.println(loginForm.toString());

        UserDetails userDetails;
        try {
            userDetails = userDetailsService.loadUserByUsername(loginForm.getUsername());
        } catch (UsernameNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not found");
        }
        if (passwordEncoder.matches(loginForm.getPassword(), userDetails.getPassword())) {
            // define fields map for claims
            Map<String, String> claims = new HashMap<>();
            // setting roles
            String authorities = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.joining(" "));
            // adding fields into encode json
            claims.put("username", loginForm.getUsername());
            claims.put(WebSecurityConfig.AUTHORITIES_CLAIM_NAME, authorities);
            claims.put("userId", String.valueOf(1));
            Optional<User> user = userService.getUserByUsername(userDetails.getUsername());
            // create jwt using JwtHelper component
            String jwt = jwtHelper.createJwtForClaims(loginForm.getUsername(), claims);
            return new ResponseEntity<>(new JwtDTO(jwt, user.get().getId(), user.get().getUsername(), user.get().getEmail(), user.get().getRole()), HttpStatus.OK);
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not authenticated");
    }

    @GetMapping("/error")
    /**
     * Error endpoint trigger when user execute some unexpected event
     * @return ResponseEntity<String> return String with error description
     */
    public ResponseEntity<String> error() {
        return new ResponseEntity<>("Something went wrong", HttpStatus.BAD_REQUEST);
    }
}
