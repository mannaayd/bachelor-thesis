package cz.cvut.fit.mannaayd.bachelor.backend.model.authorities;

import org.springframework.security.core.GrantedAuthority;

public class ManagerAuthority implements GrantedAuthority {

    @Override
    public String getAuthority() {
        return "MANAGER";
    }
}
