package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Question service interface for operations with Question entity
 */
public interface IQuestionService {
    List<Question> getAll();
    Optional<Question> getQuestionById(Long questionId);
    Question save(Question question);
    Question save(QuestionDTO questionDTO) throws NotExistsException, BadRequestException;
    boolean deleteQuestionById(Long questionId) throws NotExistsException;
    List<Question> getQuestionsByUserId(Long userId) throws NotExistsException;
    List<Question> getAnsweredQuestionsByUserId(Long userId) throws NotExistsException;
    List<Question> getUnansweredQuestionsByUserId(Long userId) throws NotExistsException;
    Question addUsersFromRegion(Long questionId, String region) throws NotExistsException;
    Question addUsersFromRegionAndCity(Long questionId, String region, String city) throws NotExistsException;
}
