package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result.AnswerResDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result.QuestionResDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result.ResultDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result.UserResDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
/**
 * Maps Result to ResultDTO and ResultDTO to Result
 */
@Component
public class ResultMapper {

    ResultMapper(){}

    public ResultDTO getResult(Survey survey){
        List<QuestionResDTO> questions = new ArrayList<>();
        for(Question question: survey.getQuestions()){
            questions.add(getQuestion(question));
        }
        return new ResultDTO(survey.getNews().getTweetId(), survey.getNews().getCity(), survey.getNews().getRegion(), questions);
    }

    QuestionResDTO getQuestion(Question question){
        List<AnswerResDTO> answers = new ArrayList<>();
        for(Answer answer: question.getAnswers()){
            answers.add(getAnswer(answer));
        }
        return new QuestionResDTO(question.getDescription(), answers);
    }

    AnswerResDTO getAnswer(Answer answer){
        return new AnswerResDTO(answer.getAnswer110(), answer.getAnswer(), getUser(answer.getUser()));
    }

    UserResDTO getUser(User user){
        return new UserResDTO(user.getUsername(), user.getEmail(), user.getRank(), user.getRegion(), user.getCity());
    }
}
