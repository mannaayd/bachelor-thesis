package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.UserDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.AnswerRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.QuestionRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.SurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
/**
 * Maps User to UserDTO and UserDTO to User, also can encode list of Users into list of UserDTO
 */
@Component
public class UserMapper implements IMapper<User, UserDTO> {
    public QuestionRepository questionRepository;
    public SurveyRepository surveyRepository;
    public AnswerRepository answerRepository;

    @Autowired
    public UserMapper(QuestionRepository questionRepository, SurveyRepository surveyRepository, AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.surveyRepository = surveyRepository;
        this.answerRepository = answerRepository;
    }

    @Override
    public User toEntity(UserDTO userDTO) throws NotExistsException {
        List<Question> questions = new LinkedList<Question>();
        for (Long questionId: userDTO.getQuestionIds()) {
            Optional<Question> question = questionRepository.findById(questionId);
            if(question.isPresent()) questions.add(question.get());
        }
        List<Survey> surveys = new LinkedList<Survey>();
        for (Long surveyId: userDTO.getSurveyIds()) {
            Optional<Survey> survey = surveyRepository.findById(surveyId);
            if(survey.isPresent()) surveys.add(survey.get());
        }
        List<Answer> answers = new LinkedList<Answer>();
        for (Long answerId: userDTO.getAnswerIds()) {
            Optional<Answer> answer = answerRepository.findById(answerId);
            if(answer.isPresent()) answers.add(answer.get());
        }
        return new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getEmail(), userDTO.getRank(), userDTO.getRegion(), userDTO.getCity(), userDTO.getRole(), questions, surveys, answers);
    }

    @Override
    public UserDTO toDTO(User user) {
        List<Long> questionIds = new LinkedList<Long>();
        List<Long> surveyIds = new LinkedList<Long>();
        List<Long> answerIds = new LinkedList<Long>();
        for (Question question: user.getQuestions()) {
            questionIds.add(question.getId());
        }
        for (Answer answer: user.getAnswers()) {
            answerIds.add(answer.getId());
        }
        for (Survey survey: user.getSurveys()) {
            surveyIds.add(survey.getId());
        }
        return new UserDTO(user.getId(), user.getUsername(), user.getEmail(), user.getRole(), user.getRank(), user.getRegion(), user.getCity(), questionIds, surveyIds, answerIds);
    }
}
