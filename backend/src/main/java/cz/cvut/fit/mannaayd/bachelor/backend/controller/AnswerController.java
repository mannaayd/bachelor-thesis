package cz.cvut.fit.mannaayd.bachelor.backend.controller;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.AnswerMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * Answer controller that handles operations with Answer entity
 */
@RestController
@CrossOrigin
public class AnswerController implements IController<AnswerDTO> {
    final private  AnswerMapper answerMapper;
    final private AnswerService answerService;

    @Autowired
    public AnswerController(AnswerMapper _answerMapper, AnswerService _answerService) {
        answerMapper = _answerMapper;
        answerService = _answerService;
    }

    /**
     * Gets all answers
     * @return list of answers
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/answers")
    public ResponseEntity<List<AnswerDTO>> all() {
        try {
            List<Answer> answers = answerService.getAll();
            return new ResponseEntity<>(answerMapper.toListDTO(answers), HttpStatus.OK);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Get answer by id
     * @param id
     * @return answer with id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/answers/{id}")
    public ResponseEntity<AnswerDTO> byID(@PathVariable Long id) {
        try {
            Optional<Answer> answer = answerService.getAnswerById(id);
            if(answer.isEmpty()) throw new NotExistsException("Answer");
            return new ResponseEntity<>(answerMapper.toDTO(answer.get()), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update Answer in database
     * @param id
     * @param answerDTO
     * @return updated answer
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/answers/{id}")
    public ResponseEntity<AnswerDTO> updateById(@PathVariable Long id, @RequestBody AnswerDTO answerDTO) {
        try {
            Optional<Answer> answer = answerService.getAnswerById(id);
            if(answer.isEmpty()) throw new NotExistsException("Answer");
            Answer updatedAnswer = answerMapper.toEntity(answerDTO);
            updatedAnswer.setId(id);
            answerService.save(updatedAnswer);
            return new ResponseEntity<>(answerDTO, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create Answer from DTO
     * @param answerDTO
     * @return created answer
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @PostMapping("/answers")
    public ResponseEntity<AnswerDTO> create(@RequestBody AnswerDTO answerDTO) {
        try {
            answerService.save(answerDTO);
            return new ResponseEntity<>(answerDTO, HttpStatus.OK);
        } catch (NotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete answer from db
     * @param id
     */
    @Override
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/answers/{id}")
    public void delete(@PathVariable Long id) {
        answerService.deleteAnswerById(id);
    }
}
