package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import org.springframework.lang.NonNull;

public class JwtDTO {
    @NonNull
    public String jwt;
    public Long id;
    public String username;
    public String email;
    public String role;

    public JwtDTO(String jwt, Long id, String username, String email, String role) {
        this.username = username;
        this.email = email;
        this.role = role;
        this.jwt = jwt;
        this.id = id;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
