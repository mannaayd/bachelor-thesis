package cz.cvut.fit.mannaayd.bachelor.backend.model.dto;

import java.util.List;

public class SurveyDTO {

    public Long Id;
    public Long newsId;
    public Long authorId;
    public List<Long> questionIds;
    public boolean ended = false;

    public SurveyDTO(Long id, Long newsId, Long authorId, List<Long> questionIds, boolean isEnded) {
        Id = id;
        this.newsId = newsId;
        this.authorId = authorId;
        this.questionIds = questionIds;
        this.ended = isEnded;
    }

    public SurveyDTO() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Long> questionIds) {
        this.questionIds = questionIds;
    }

    public boolean getEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }
}
