package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;

import java.util.List;
import java.util.Optional;

/**
 * Answer service interface for operations with Answer entity
 */
public interface IAnswerService {
    List<Answer> getAll();
    Optional<Answer> getAnswerById(Long answerId);
    Answer save(Answer answer);
    Answer save(AnswerDTO answerDTO) throws NotExistsException, BadRequestException;
    List<Answer> getAnswersByUserId(Long userId) throws NotExistsException;
    boolean deleteAnswerById(Long answerId) throws NotExistsException;
}
