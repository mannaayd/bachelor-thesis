package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;

import java.util.List;
import java.util.Optional;

/**
 * News service interface for operations with News entity
 */
public interface INewsService {
    List<News> getAll();
    Optional<News> getNewsById(Long newsId);
    News save(News news);
    News save(NewsDTO newsDTO)  throws NotExistsException;
    boolean deleteNewsById(Long newsId) throws NotExistsException;
}
