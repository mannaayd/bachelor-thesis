package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.SurveyDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.UserDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.NewsRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.QuestionRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
/**
 * Maps Survey to SurveyDTO and SurveyDTO to Survey, also can encode list of Surveys into list of SurveyDTO
 */
@Component
public class SurveyMapper implements IMapper<Survey, SurveyDTO> {
    public UserRepository userRepository;
    public QuestionRepository questionRepository;
    public NewsRepository newsRepository;
    public UserMapper userMapper;
    public QuestionMapper questionMapper;
    public NewsMapper newsMapper;

    @Autowired
    public SurveyMapper(UserRepository userRepository, QuestionRepository questionRepository, NewsRepository newsRepository, UserMapper userMapper, QuestionMapper questionMapper, NewsMapper newsMapper) {
        this.userRepository = userRepository;
        this.questionRepository = questionRepository;
        this.newsRepository = newsRepository;
        this.userMapper = userMapper;
        this.questionMapper = questionMapper;
        this.newsMapper = newsMapper;
    }


    @Override
    public Survey toEntity(SurveyDTO surveyDTO) throws NotExistsException {
        List<Question> questions = new LinkedList<Question>();
        for (Long questionId : surveyDTO.getQuestionIds()) {
            Optional<Question> question = questionRepository.findById(questionId);
            if(question.isPresent()) questions.add(question.get());
        }
        Optional<News> news = newsRepository.findById(surveyDTO.getNewsId());
        if(news.isEmpty()) throw new NotExistsException("News");
        Optional<User> author = userRepository.findById(surveyDTO.getAuthorId());
        if(author.isEmpty()) throw new NotExistsException("Author"); // todo add author from context of authorization
        return new Survey(news.get(), questions, author.get(), surveyDTO.getEnded());
    }

    @Override
    public SurveyDTO toDTO(Survey survey) {
        List<Long> questionIds = new LinkedList<Long>();
        for (Question question: survey.getQuestions()) {
            questionIds.add(question.getId());
        }
        return new SurveyDTO(survey.getId(), survey.getNews().getId(), survey.getAuthor().getId(), questionIds, survey.isEnded());
    }
}
