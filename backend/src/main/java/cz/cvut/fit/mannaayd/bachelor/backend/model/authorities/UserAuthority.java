package cz.cvut.fit.mannaayd.bachelor.backend.model.authorities;

import org.springframework.security.core.GrantedAuthority;

public class UserAuthority implements GrantedAuthority {
    @Override
    public String getAuthority() {
        return "USER";
    }
}
