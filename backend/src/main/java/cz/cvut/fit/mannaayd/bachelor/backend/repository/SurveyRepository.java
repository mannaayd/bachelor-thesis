package cz.cvut.fit.mannaayd.bachelor.backend.repository;

import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Survey repository to communicate with database via Hibernate
 */
public interface SurveyRepository extends JpaRepository<Survey, Long > {
    @Override
    Optional<Survey> findById(Long id);

    @Override
    List<Survey> findAll();


}
