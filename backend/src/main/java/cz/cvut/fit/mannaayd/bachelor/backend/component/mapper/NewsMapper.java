package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.News;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.NewsDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.SurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
/**
 * Maps News to NewsDTO and NewsDTO to News, also can encode list of Newss into list of NewsDTO
 */
@Component
public class NewsMapper implements IMapper<News, NewsDTO> {

    public SurveyRepository surveyRepository;

    @Autowired
    public NewsMapper(SurveyRepository surveyRepository) {
        this.surveyRepository = surveyRepository;
    }

    @Override
    public News toEntity(NewsDTO newsDTO) throws NotExistsException {
        if(newsDTO.getSurveyId() != null){
            Optional<Survey> survey = surveyRepository.findById(newsDTO.getSurveyId());
            if(survey.isEmpty()) throw new NotExistsException("Survey");
            return new News(newsDTO.getTweetId(), newsDTO.getType(), newsDTO.getCity(), newsDTO.getRegion(), survey.get());
        }
        return new News(newsDTO.getTweetId(), newsDTO.getType(), newsDTO.getCity(), newsDTO.getRegion(), null);
    }

    @Override
    public NewsDTO toDTO(News news) {
        return new NewsDTO(news.getId(), news.getTweetId(), news.getType(), news.getCity(), news.getRegion(), news.getSurvey() == null ? null : news.getSurvey().getId());
    }
}
