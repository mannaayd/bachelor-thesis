package cz.cvut.fit.mannaayd.bachelor.backend.service;

import cz.cvut.fit.mannaayd.bachelor.backend.component.mapper.AnswerMapper;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.AnswerRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerService implements IAnswerService {
    public AnswerMapper answerMapper;
    public AnswerRepository answerRepository;
    public UserRepository userRepository;

    @Autowired
    public AnswerService(AnswerMapper _answerMapper, AnswerRepository _answerRepository, UserRepository userRepository) {
        answerMapper = _answerMapper;
        answerRepository = _answerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Answer> getAll() {
        return answerRepository.findAll();
    }

    @Override
    public Optional<Answer> getAnswerById(Long answerId)  {
        return answerRepository.findById(answerId);
    }

    @Override
    public Answer save(Answer answer) {
        return answerRepository.save(answer);
    }

    @Override
    public Answer save(AnswerDTO answerDTO) throws NotExistsException, BadRequestException {
        return answerRepository.save(answerMapper.toEntity(answerDTO));
    }

    @Override
    public List<Answer> getAnswersByUserId(Long userId) throws NotExistsException{
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()) throw new NotExistsException("User");
        return user.get().getAnswers();
    }

    @Override
    public boolean deleteAnswerById(Long answerId) {
        Optional<Answer> answer = answerRepository.findById(answerId);
        if(answer.isEmpty()) return false;
        answerRepository.delete(answer.get());
        return true;
    }
}
