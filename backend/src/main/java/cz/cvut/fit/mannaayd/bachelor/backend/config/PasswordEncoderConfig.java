package cz.cvut.fit.mannaayd.bachelor.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
/**
 * Password encoder for safely storing passwords
 */
public class PasswordEncoderConfig {
    /**
     * Builds password encoder
     * @return password encoder
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
