package cz.cvut.fit.mannaayd.bachelor.backend.model.dto.result;

public class AnswerResDTO {
    public int answer110;
    public String answer;
    public UserResDTO user;

    public AnswerResDTO(int answer110, String answer, UserResDTO user) {
        this.answer110 = answer110;
        this.answer = answer;
        this.user = user;
    }

    public int getAnswer110() {
        return answer110;
    }

    public void setAnswer110(int answer110) {
        this.answer110 = answer110;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public UserResDTO getUser() {
        return user;
    }

    public void setUser(UserResDTO user) {
        this.user = user;
    }
}
