package cz.cvut.fit.mannaayd.bachelor.backend.controller;


import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Interface that declares basic CRUD operations on entities
 * @param <DTO> DTO class that returns to client
 */
public interface IController<DTO>{
    /**
     * Gets all entities
     * @return list of entities
     */
    ResponseEntity<List<DTO>>   all();

    /**
     * Get entity from db by id
     * @param id
     * @return entity
     */
    ResponseEntity<DTO>         byID(Long id);

    /**
     * Update entity in db by id
     * @param id
     * @param dto
     * @return updated entity
     */
    ResponseEntity<DTO>         updateById(Long id, DTO dto);

    /**
     * Create entity and store it in db
     * @param dto
     * @return created entity
     */
    ResponseEntity<DTO>         create(DTO dto);

    /**
     * Delete entity from db
     * @param id
     */
    void                        delete(Long id);
}
