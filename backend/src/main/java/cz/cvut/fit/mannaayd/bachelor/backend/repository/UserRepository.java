package cz.cvut.fit.mannaayd.bachelor.backend.repository;

import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * User repository to communicate with database via Hibernate
 */
public interface UserRepository extends JpaRepository<User, Long> {
    @Override
    Optional<User> findById(Long id);

    @Override
    List<User> findAll();

    Optional<User> findByUsername(String username);

    List<User> findUsersByRegionAndCity(String region, String city);

    List<User> findUsersByRegion(String region);
}
