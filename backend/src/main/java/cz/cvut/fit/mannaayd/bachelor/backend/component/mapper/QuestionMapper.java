package cz.cvut.fit.mannaayd.bachelor.backend.component.mapper;

import cz.cvut.fit.mannaayd.bachelor.backend.exception.BadRequestException;
import cz.cvut.fit.mannaayd.bachelor.backend.exception.NotExistsException;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Question;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Survey;
import cz.cvut.fit.mannaayd.bachelor.backend.model.Answer;
import cz.cvut.fit.mannaayd.bachelor.backend.model.User;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.AnswerDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.model.dto.QuestionDTO;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.AnswerRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.SurveyRepository;
import cz.cvut.fit.mannaayd.bachelor.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
/**
 * Maps News to NewsDTO and NewsDTO to News, also can encode list of News into list of NewsDTO
 */
@Component
public class QuestionMapper implements IMapper<Question, QuestionDTO> {
    public AnswerMapper answerMapper;
    public AnswerRepository answerRepository;
    public UserRepository userRepository;
    public SurveyRepository surveyRepository;

    @Autowired
    public QuestionMapper(AnswerMapper answerMapper, AnswerRepository answerRepository, UserRepository userRepository, SurveyRepository surveyRepository) {
        this.answerMapper = answerMapper;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
        this.surveyRepository = surveyRepository;
    }


    @Override
    public Question toEntity(QuestionDTO questionDTO) throws NotExistsException, BadRequestException {
        List<Answer> answers = new LinkedList<Answer>();
        for(Long answerId : questionDTO.getAnswerIds()) {
            Optional<Answer> answer = answerRepository.findById(answerId);
            if(answer.isPresent()) answers.add(answer.get());
        }
        List<User> respondents = new LinkedList<User>();
        for (Long respondentId : questionDTO.getRespondentIds()) {
            Optional<User> respondent = userRepository.findById(respondentId);
            if(respondent.isPresent()) respondents.add(respondent.get());
        }
        Optional<Survey> survey = surveyRepository.findById(questionDTO.getSurveyId());
        if(survey.isEmpty()) throw new NotExistsException("Survey");
        if(survey.get().isEnded()) throw new BadRequestException("Survey ended.");
        return new Question(questionDTO.getDescription(), survey.get(), respondents, answers);
    }

    @Override
    public QuestionDTO toDTO(Question question) {
        List<Long> respondentIds = new LinkedList<Long>();
        List<Long> answerIds = new ArrayList<Long>();
        for (User user: question.getRespondents()) {
            respondentIds.add(user.getId());
        }
        for (Answer answer: question.getAnswers()) {
            answerIds.add(answer.getId());
        }
        return new QuestionDTO(question.getId(), question.getDescription(), question.getSurvey().getId(), respondentIds, answerIds);
    }
}
