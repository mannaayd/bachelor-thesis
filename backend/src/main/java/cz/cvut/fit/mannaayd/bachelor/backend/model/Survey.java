package cz.cvut.fit.mannaayd.bachelor.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "survey")
public class Survey {
    @Id
    @GeneratedValue
    private Long Id = 0L;


    @ManyToOne
    @JoinColumn(nullable = false)
    private News news;

    @OneToMany(mappedBy = "survey", fetch = FetchType.EAGER)
    private List<Question> questions;

    private boolean isEnded = false;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User author;

    private float result = 0;

    public Survey(){

    }

    public Survey(News news, List<Question> questions, User author, boolean isEnded) {
        this.news = news;
        this.questions = questions;
        this.author = author;
        this.isEnded = isEnded;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public boolean isEnded() {
        return isEnded;
    }

    public void setEnded(boolean ended) {
        isEnded = ended;
    }
}
