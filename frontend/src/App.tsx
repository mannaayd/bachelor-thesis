import { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import AuthService from "./services/AuthService";
import {BoardState} from "./types/states/BoardState";
import EventHandler from "./services/EventHandler";
import {Container, Nav, Navbar} from "react-bootstrap";

type Props = {
}

class App extends Component<Props, BoardState> {
  constructor(props: Props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.state = {
      showManagerBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };
  }
  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showManagerBoard: user.role === "MANAGER" || user.role === "ADMIN",
        showAdminBoard: user.role === "ADMIN",
      });
    }
    EventHandler.on("logout", this.logout);
  }
  componentWillUnmount() {
    EventHandler.remove("logout", this.logout);
  }
  logout() {
    AuthService.logout();
    this.setState({
      showManagerBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    });
  }
  render() {
    const { currentUser, showManagerBoard, showAdminBoard } = this.state;
    return (
        <Navbar bg="light" variant="light" >
          <Container>
            <Navbar.Brand href="/home">News verification app</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link href="/home">Home</Nav.Link>
              { !currentUser && <Nav.Link href="/login">Login</Nav.Link> }
              { !currentUser && <Nav.Link href="/register">Register</Nav.Link> }
              { currentUser && <Nav.Link href="/profile">Profile</Nav.Link> }
              { currentUser && <Nav.Link href="/user">UserBoard</Nav.Link> }
              { showManagerBoard && <Nav.Link href="/manager">ManagerBoard</Nav.Link> }
              { showAdminBoard && <Nav.Link href="/admin">AdminBoard</Nav.Link> }
              { currentUser && <Nav.Link href="/login" onClick={this.logout}>Logout</Nav.Link> }
            </Nav>
          </Container>
        </Navbar>

    );
  }
}
export default App;