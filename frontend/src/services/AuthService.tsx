import axios from "axios";
import UserType from "../types/UserType";
const API_URL = "http://localhost:8080/";

class AuthService {
  login(username: string, password: string) {
    var headers = {headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
      }}
    return axios
      .post(API_URL + "login", {
        "username": username,
        "password": password
      }, headers)
      .then(response => {
        //console.log(response.data);

        if (response.data.jwt) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }
        return response.data;
      });
  }
  logout() {
    localStorage.removeItem("user");
  }

  register(username: string, email: string, password: string, city: string, region: string) {
    var headers = {headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
      }}
    return axios.post(API_URL + "register", {
      "username": username,
      "email": email,
      "password": password,
      "city": city,
      "region": region
    }, headers);
  }

  getCurrentUser() : UserType | null {
    const userStr = localStorage.getItem("user");
    if (userStr) return JSON.parse(userStr);
    return null;
  }
}
export default new AuthService();