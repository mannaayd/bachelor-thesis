import axios from "axios";
import authHeader from "./AuthHeader";
import {Question} from "../types/models/Question";

const API_URL = 'http://localhost:8080/';
class QuestionService {
    getQuestionTweet(id?: number) {
        // @ts-ignore
        return axios.get(API_URL + 'questions/' + id + '/tweet',  authHeader())
    }

    postQuestion(question: Question) {
        return axios.post(API_URL + 'questions', question, authHeader())
    }
} export default new QuestionService;