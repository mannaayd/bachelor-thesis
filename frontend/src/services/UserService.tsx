import axios, { AxiosRequestHeaders } from 'axios';
import authHeader from './AuthHeader';
import AuthService from "./AuthService";
import {Answer} from "../types/models/Answer"
import UserType from "../types/UserType";
const API_URL = 'http://localhost:8080/';
class UserService {
  getAllUsers(){
    return axios.get(API_URL + 'users',  authHeader());
  }

  getUserQuestions() {
    var user: UserType | null = AuthService.getCurrentUser()
    // @ts-ignore
    return axios.get(API_URL + 'users/' + user.id + '/questions',  authHeader())
  }

  getUserAnsweredQuestions() {
    var user: UserType | null = AuthService.getCurrentUser()
    // @ts-ignore
    return axios.get(API_URL + 'users/' + user.id + '/questions/answered',  authHeader())
  }

  getUserUnansweredQuestions() {
    var user: UserType | null = AuthService.getCurrentUser()
    // @ts-ignore
    return axios.get(API_URL + 'users/' + user.id + '/questions/unanswered',  authHeader())
  }

  getUserSurveys() {
    var user: UserType | null = AuthService.getCurrentUser()
    // @ts-ignore
    return axios.get(API_URL + 'users/' + user.id + '/surveys',  authHeader())
  }

  getUserAnswers() {
    var user: UserType | null = AuthService.getCurrentUser()
    // @ts-ignore
    return axios.get(API_URL + 'users/' + user.id + '/answers',  authHeader())
  }

  postAnswer(answer: Answer) {
    return axios.post(API_URL + 'answers', answer, authHeader());
  }
}
export default new UserService();