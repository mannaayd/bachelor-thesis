import axios from "axios";
import authHeader from "./AuthHeader";
import AuthService from "./AuthService";
import {News} from "../types/models/News";
import {Survey} from "../types/models/Survey";

const API_URL = 'http://localhost:8080/';
class SurveyService {
    createNews(news: News) {
        // @ts-ignore
        return axios.post(API_URL + 'news/', news,  authHeader())
    }

    startSurvey(news: News) {
        let survey: Survey = {
            newsId: news.id,
            authorId: AuthService.getCurrentUser()?.id,
            questionIds: [],
            ended: false
        }
        return axios.post(API_URL + 'surveys/', survey,  authHeader())
    }

    endSurvey(survey: Survey) {
        survey.ended = true;

    }

    updateSurvey(survey: Survey) {
        return axios.put(API_URL + 'surveys/' + survey.id, survey, authHeader())
    }

    getAllSurveys(){
        return axios.get(API_URL + 'surveys', authHeader())
    }

    getSurveyTweet(id: number| undefined) {
        // @ts-ignore
        return axios.get(API_URL + 'surveys/' + id + '/news',  authHeader())
    }

    getSurveyQuestions(id: number| undefined) {
        return axios.get(API_URL + 'surveys/' + id + '/questions',  authHeader())
    }

    getSurveyResults(id: number| undefined) {
        return axios.get(API_URL + 'surveys/' + id + '/results',  authHeader())
    }


} export default new SurveyService;