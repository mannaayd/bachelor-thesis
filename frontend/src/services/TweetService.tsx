import axios from "axios";
import authHeader from "./AuthHeader";

const API_URL = 'http://localhost:8080/';
class TweetService {
    getLastTweets(count: number) {
        // @ts-ignore
        return axios.get(API_URL + 'news/last/' + count,  authHeader())
    }
} export default new TweetService;