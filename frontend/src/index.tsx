import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes  } from "react-router-dom";
import './index.css';
import Login from "./components/LoginComponent";
import Register from "./components/RegisterComponent";
import Home from "./components/HomePageComponent";
import Profile from "./components/ProfileComponent";
import App from './App';
import {UserBoard} from "./components/boards/UserBoard";
import {AdminBoard} from "./components/boards/AdminBoard";
import {ManagerBoard} from "./components/boards/ManagerBoard";

ReactDOM.render(
    <BrowserRouter>
        <App />
        <Routes>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/home" element={<Home/>}/>
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/user" element={<UserBoard/>}/>
            <Route path="/admin" element={<AdminBoard/>}/>
            <Route path="/manager" element={<ManagerBoard/>}/>
        </Routes>
    </BrowserRouter>,
    document.getElementById('root')
);

