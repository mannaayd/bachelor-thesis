import { Component } from "react";
import UserService from "../services/UserService";
type State = {
  content: string
  text: string
}

type Props = {}

export default class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      content: "News verification app",
      text: "This is a prototype news verification app. This app pulls tweets from twitter and making surveys."
    };
  }
  componentDidMount() {

  }
  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <h3>{this.state.content}</h3>
        </header>
        {this.state.text}
      </div>
    );
  }
}