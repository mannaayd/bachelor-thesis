import {Component} from "react";
import {ManagerBoardState} from "../../types/states/ManagerBoardState";
import {Accordion, Container} from "react-bootstrap";
import TweetService from "../../services/TweetService";
import TweetCard from "../cards/TweetCard";
import SurveyService from "../../services/SurveyService";
import SurveyCard from "../cards/SurveyCard";

type Props = {}

export class ManagerBoard extends Component<Props, ManagerBoardState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            tweets: {data: []},
            surveys: []
        }

    }

    componentDidMount() {
        TweetService.getLastTweets(20).then((response) =>{
            this.setState({
                tweets: response.data
            })
        })
        SurveyService.getAllSurveys().then((response)=>{
            this.setState({
                surveys: response.data
            })
        })
    }

    render() {
        const lastTweets = this.state.tweets.data.map((tweet, key) => <TweetCard tweet={tweet} key={key}/>)
        const endedSurveys = this.state.surveys.map((survey, key) => {if(survey.ended) return <SurveyCard survey={survey} key={key}/>; } )
        const activeSurveys = this.state.surveys.map((survey, key) => {if(!survey.ended) return <SurveyCard survey={survey} key={key}/>; } )
        return (
            <Container>
                <Accordion >
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>Last tweets(start survey)</Accordion.Header>
                        <Accordion.Body>
                            {lastTweets}
                            {/*Here i will add(start) new survey  for selected tweet, also here i will add questions*/}
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>Active surveys</Accordion.Header>
                        <Accordion.Body>
                            {activeSurveys}
                            {/*Here is I will get all my surveys, also here I will be able to modify survey/questions, and stop survey*/}
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                    <Accordion.Header>Ended surveys</Accordion.Header>
                        <Accordion.Body>
                            {endedSurveys}
                            {/*Here I will get all my surveys, also here I will be able to modify survey/questions, and stop survey*/}
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Container>
        );
    }
}