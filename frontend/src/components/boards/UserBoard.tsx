import {Component} from "react";
import {UserBoardState} from "../../types/states/UserBoardState";
import {Container, Accordion} from "react-bootstrap";
import UserService from "../../services/UserService";
import QuestionCard from "../cards/QuestionCard";
import authService from "../../services/AuthService";

type Props = {};

export class UserBoard extends Component<Props, UserBoardState>
{
    constructor(props: Props){
        super(props)
        this.state = {
            content: "init",
            answeredQuestions: null,
            unansweredQuestions: null,
            surveys: null,
            answers: null,
            userId: authService.getCurrentUser()?.id
        }


    }

    async componentDidMount(){

        await UserService.getUserAnsweredQuestions()
            .then(resolve => {
                  if(resolve !== undefined) this.setState({answeredQuestions: resolve.data})
                })
                .catch(error => console.log("error"))
        await UserService.getUserUnansweredQuestions()
            .then(resolve => {
                if(resolve !== undefined) this.setState({unansweredQuestions: resolve.data})
            })
            .catch(error => console.log("error"))

    }

    render() {
        const answeredQuestions = this.state.answeredQuestions?.map((question, key) => <QuestionCard question={question} showTweet={true} answered={true} userId={this.state.userId} key={key}/>)
        const unansweredQuestions = this.state.unansweredQuestions?.map((question, key) => <QuestionCard question={question} showTweet={true} answered={false} userId={this.state.userId} key={key}/>)
        return (
            <Container>
                <Accordion >
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>Questions</Accordion.Header>
                        <Accordion.Body>
                            {unansweredQuestions}
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>Answered/Skipped questions</Accordion.Header>
                        <Accordion.Body>
                            {answeredQuestions}
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Container>
        );
    }
}