import {Component} from "react";
import {AdminBoardState} from "../../types/states/AdminBoardState";
import {Accordion, Container} from "react-bootstrap";
import UserService from "../../services/UserService";
import UserCard from "../cards/UserCard";

type Props = {}

export class AdminBoard extends Component<Props, AdminBoardState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        UserService.getAllUsers().then((response) => {
            this.setState({users: response.data})
        })
    }

    render() {
        const users = this.state.users.map((user, key) => <UserCard user={user} key={key}/>)
        return (
            <Container>
                <Accordion >
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>Users</Accordion.Header>
                        <Accordion.Body>
                            {users}
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <Accordion.Header>Another admin stuff</Accordion.Header>
                        <Accordion.Body>
                            Another stuff....
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Container>
        );
    }
}