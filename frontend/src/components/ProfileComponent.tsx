import { Component } from "react";
import { Navigate } from "react-router-dom";
import AuthService from "../services/AuthService";
import {ProfileState} from "../types/states/ProfileState";

type Props = {}

export default class Profile extends Component<Props, ProfileState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      redirect: null,
      userReady: false,
      currentUser: null
    };
  }
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
    let reloadCount: number = 0;
    if(sessionStorage.getItem('reloadCount')) { // @ts-ignore
      reloadCount = parseInt(sessionStorage.getItem('reloadCount'), 10);
    }
    if(reloadCount < 2) {
      sessionStorage.setItem('reloadCount', String(reloadCount + 1));
      window.location.reload();
    } else {
      sessionStorage.removeItem('reloadCount');
    }
  }

  render() {
    if (this.state.redirect) {
      return <Navigate replace={true} to={this.state.redirect} />
    }
    const { currentUser } = this.state;
    return (
      <div className="container">
        {(this.state.userReady) ?
          <div>
            <header className="jumbotron">
              <h3>
                <strong>{currentUser && currentUser.username}</strong> Profile
              </h3>
            </header>
            <p>
              <strong>Token:</strong>{" "}
              {currentUser && currentUser.jwt.substring(0, 20)} ...{" "}
              {currentUser && currentUser.jwt.substr(currentUser.jwt.length - 20)}
            </p>
            <p>
              <strong>Id:</strong>{" "}
              {currentUser && currentUser.id}
            </p>
            <p>
              <strong>Email:</strong>{" "}
              {currentUser && currentUser.email}
            </p>
            <p>
              <strong>Role:</strong>
                {currentUser && currentUser.role}
            </p>
          </div> : null}
      </div>
    );
  }
}