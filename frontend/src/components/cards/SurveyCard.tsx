import {News} from "../../types/models/News";
import {Component} from "react";
import {Button, Card, Form, Modal} from "react-bootstrap";
import {TwitterTweetEmbed} from "react-twitter-embed";
import {Survey} from "../../types/models/Survey";
import SurveyService from "../../services/SurveyService";
import {Multiselect} from "multiselect-react-dropdown";
import {User} from "../../types/models/User";
import UserService from "../../services/UserService";
import QuestionService from "../../services/QuestionService";
import {Question} from "../../types/models/Question";
import surveyService from "../../services/SurveyService";
import QuestionCard from "./QuestionCard";

type Props = {survey: Survey}

type SurveyCardState = {
    survey: Survey,
    news: News,
    show: boolean,
    showDialog: boolean,
    question: string,
    users: User[],
    selectedUsers: User[],
    questions: Question[]
}
export default class SurveyCard extends Component<Props, SurveyCardState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            survey: props.survey,
            news: {tweetId: "", region: "", city: "", type: ""},
            show: true,
            showDialog: false,
            question: "",
            users: [],
            selectedUsers: [],
            questions: []
        }
    }

    componentDidMount() {
        SurveyService.getSurveyTweet(this.state.survey.id).then((response)=>{
            this.setState({news: response.data})
        })
        UserService.getAllUsers().then((response)=>{
            this.setState({users: response.data})
        })
        SurveyService.getSurveyQuestions(this.state.survey.id).then((response)=>{
            this.setState({questions: response.data})
        })
    }

    render() {
        const handleClose = () => this.setState({showDialog: false});
        const handleQuestionChange = (e: { target: { value: any; }; }) => {
            this.setState({ question: e.target.value });
        };
        const handleSubmitModal = () => {
            var question: Question = {
                description: this.state.question,
                surveyId: this.state.survey.id,
                respondentIds: this.state.users.map((user)=> user.Id),
                answerIds: []
            }
            QuestionService.postQuestion(question).then((response) => {
                this.setState({showDialog: false})
            })
        }
        const onSelect = (selectedList: any, selectedItem: any) => {
            this.setState({selectedUsers: selectedList})
        }

        const onRemove = (selectedList: any, removedItem: any) => {
            selectedList = selectedList.filter((e: any) => {
                return e !== removedItem
            })
            this.setState({selectedUsers: selectedList})
        }

        const handleEndSurvey = () => {
            var survey = this.state.survey;
            survey.ended = true
            surveyService.updateSurvey(survey)
            this.setState({show: false})
        }
        const questions = this.state.questions?.map((question, key) => <QuestionCard question={question} showTweet={false} answered={true} userId={0} key={key}/>)
        const handleShowDialog = () => this.setState({showDialog: true});
        const tweetId = this.state.news.tweetId;
        const handleResults = () => {
            SurveyService.getSurveyResults(this.state.survey.id).then((response)=>{
                const blob: Blob = new Blob([JSON.stringify(response.data)], { type: 'text/json' })
                const a = document.createElement('a')
                a.download = 'results.json'
                a.href = window.URL.createObjectURL(blob)
                a.click()
            })
        }

        return (
            <div>
                {this.state.show && <Card>
                    <Card.Header>Survey</Card.Header>
                    <Card.Body>
                        {this.state.news.tweetId !== "" && <TwitterTweetEmbed tweetId={tweetId} />}
                        <Card.Text>

                        </Card.Text>
                        {!this.state.survey.ended &&
                            <div>
                                <Button variant="primary" onClick={handleShowDialog}>
                                    Add question
                                </Button>
                                {questions}
                                <Button variant="danger" onClick={handleEndSurvey}>
                                    End survey
                                </Button>
                                <Modal show={this.state.showDialog} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Add question</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <Form>
                                            <Form.Group className="mb-3" controlId="formCity">
                                                <Form.Label>Question</Form.Label>
                                                <Form.Control type="question" placeholder="Enter your question." onChange={handleQuestionChange}/>
                                                <Multiselect
                                                    placeholder="Attach users"
                                                    options={this.state.users}
                                                    selectedValues={this.state.selectedUsers}
                                                    onSelect={onSelect}
                                                    onRemove={onRemove}
                                                    displayValue="username"
                                                />

                                            </Form.Group>
                                        </Form>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Close
                                        </Button>
                                        <Button variant="primary" onClick={handleSubmitModal}>
                                            Add question
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </div>
                        }
                        {this.state.survey.ended &&
                            <Button variant="warning" onClick={handleResults}>
                                Get results
                            </Button>
                        }
                    </Card.Body>
                </Card>}
            </div>
        );
    }
}