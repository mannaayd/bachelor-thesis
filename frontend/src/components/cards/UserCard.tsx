import {Component} from "react";
import {User} from "../../types/models/User";
import {Card} from "react-bootstrap";

type Props = {
    user: User
}

type UserCardState = {
    user: User
    show: boolean
}

export default class UserCard extends Component<Props, UserCardState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            user: props.user,
            show: true
        }
    }

    componentDidMount() {

    }

    render() {
        return (this.state.show &&
            <div>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title>{this.state.user.username}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">{this.state.user.role}</Card.Subtitle>
                        <Card.Text>
                            city: {this.state.user.city}<br/>
                            region: {this.state.user.region}<br/>
                            email: {this.state.user.email}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}