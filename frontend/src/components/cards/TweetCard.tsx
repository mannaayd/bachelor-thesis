import {News} from "../../types/models/News";
import {Component} from "react";
import {Button, Card, Form, Modal} from "react-bootstrap";
import {TwitterTweetEmbed} from "react-twitter-embed";
import {Tweet} from "../../types/models/Tweet";
import TweetService from "../../services/TweetService";
import SurveyService from "../../services/SurveyService";
import axios from "axios";

type Props = {tweet: Tweet}

type TweetCardState = {
    tweet: Tweet,
    hasSurvey: boolean,
    show: boolean,
    type: string,
    city: string,
    region: string
}

export default class NewsCard extends Component<Props, TweetCardState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            tweet: this.props.tweet,
            hasSurvey: false,
            show: false,
            type: "",
            city: "",
            region: ""
        }
    }

    handleCityChange = (e: { target: { value: any; }; }) => {
        this.setState({ city: e.target.value });
    };

    handleRegionChange = (e: { target: { value: any; }; }) => {
        this.setState({ region : e.target.value });
    };

    handleTypeChange = (e: { target: { value: any; }; }) => {
        this.setState({ type: e.target.value });
    };

    componentDidMount() {

    }

    render() {
        const handleClose = () => this.setState({show: false});
        const handleSubmitModal = () => {

            // post new news and post new survey
            if (this.state.type === "" || this.state.city === "" || this.state.region === "") return;
            var news: News = {
                tweetId: this.state.tweet.id,
                region: this.state.region,
                city: this.state.city,
                type: this.state.type
            };
            SurveyService.createNews(news).then((response)=>{
                news = response.data
                SurveyService.startSurvey(news).then((response) => {
                    this.setState({show: false, hasSurvey: true});
                })
            })


        }


        const handleShow = () => this.setState({show: true});
        return (
            <div>
                {!this.state.hasSurvey && <Card>
                    <Card.Header>Tweet</Card.Header>
                    <Card.Body>
                        <TwitterTweetEmbed tweetId={this.state.tweet.id}/>
                        <Button variant="primary" onClick={handleShow}>
                            Create survey
                        </Button>

                        <Modal show={this.state.show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Create survey</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Type</Form.Label>
                                        <Form.Select aria-label="Default select example" onChange={this.handleTypeChange}>
                                            <option>Default select</option>
                                            <option value="Politics">Politics</option>
                                            <option value="Techologies">Technologies</option>
                                            <option value="Celebreties">Celebreties</option>
                                            <option value="Other">Other</option>
                                        </Form.Select>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formCity">
                                        <Form.Label>City</Form.Label>
                                        <Form.Control type="city" placeholder="Enter city you are locating." onChange={this.handleCityChange}/>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formRegion">
                                        <Form.Label>Region</Form.Label>
                                        <Form.Control type="region" placeholder="Enter region you are locating." onChange={this.handleRegionChange}/>
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button variant="primary" onClick={handleSubmitModal}>
                                    Start survey
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </Card.Body>
                </Card>}
            </div>
        );
    }
}