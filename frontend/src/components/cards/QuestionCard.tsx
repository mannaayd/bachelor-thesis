import {ChangeEvent, Component} from "react";
import {Question} from "../../types/models/Question";
import {Button, Card, Form} from "react-bootstrap";
import userService from "../../services/UserService";
import {TwitterTweetEmbed} from "react-twitter-embed";
import QuestionService from "../../services/QuestionService";
import {News} from "../../types/models/News";

type Props = {question: Question, answered: boolean, userId: number | undefined, showTweet: boolean}

type QuestionState = {
    question: Question
    answered: boolean
    answer: string
    answer110: number
    userId: number | undefined
    show: boolean
    showTweet: boolean
    news: News
}


export default class QuestionCard extends Component<Props, QuestionState>
{
    constructor(props: Props) {
        super(props);
        this.state = {
            showTweet: props.showTweet,
            question: props.question,
            answered: props.answered,
            answer: "",
            answer110: 1,
            userId: props.userId,
            show: true,
            news: { tweetId: "", city: "", region: "", type: "" }
        }

        this.handleAnswer110Change = this.handleAnswer110Change.bind(this);
        this.handleAnswerChange = this.handleAnswerChange.bind(this);
        this.addAnswer = this.addAnswer.bind(this);
    }

    componentDidMount() {
        QuestionService.getQuestionTweet(this.state.question.id).then((response) =>{
            this.setState({news: response.data})
        });
    }

    addAnswer (event: any){
        event.preventDefault();
        userService.postAnswer({
            answer: this.state.answer,
            answer110: this.state.answer110,
            questionId: this.state.question.Id,
            userId: this.state.userId
        }).then((resolve)=> {
            console.log(resolve)
            this.setState({answered: true, show:false})
        })

    }

    handleAnswerChange = (e: { target: { value: any; }; }) => {
        this.setState({ answer: e.target.value });
    };

    handleAnswer110Change = (e: { target: { value: any; }; }) => {
        this.setState({ answer110: e.target.value });
    };

    render() {
        const tweetId = this.state.news.tweetId;
        return (
            <div>
                {this.state.show && <Card>
                    <Card.Header>Question</Card.Header>
                    <Card.Body>
                        {this.state.showTweet && this.state.news.tweetId !== "" && <TwitterTweetEmbed tweetId={tweetId} />}
                        <Card.Title>{this.state.question.description}</Card.Title>

                        {!this.state.answered &&
                            <Form onSubmit={this.addAnswer}>

                                <Form.Group className="mb-3" controlId="formBasic">
                                    <Form.Label>Answer</Form.Label>
                                    <Form.Control as="textarea" rows={3} type="answer" placeholder="Enter answer" onChange={this.handleAnswerChange}/>

                                </Form.Group>
                                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                    <Form.Label>Believability</Form.Label>
                                    <Form.Select aria-label="Default select example" onChange={this.handleAnswer110Change}>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                                    <Form.Check type="checkbox" label="Anonymize" />
                                </Form.Group>
                                <Button variant="primary" type="submit" >
                                    Answer
                                </Button>
                            </Form>}
                    </Card.Body>
                </Card>}
            </div>
        );
    }
}