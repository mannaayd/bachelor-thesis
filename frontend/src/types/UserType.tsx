export default interface UserType {
    jwt: string
    id: number
    username: string
    email: string
    role: string
}