export type Tweet = {
    id: string
    text: string
}

export type Tweets = {
    data: Tweet[]
}