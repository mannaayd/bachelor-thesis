export type User = {
    Id: number
    username: string
    email: string
    password: string | null
    rank: number
    region: string
    city: string
    role: string
    questionsIds: number[]
    surveyIds: number[]
    answerIds: number[]
}