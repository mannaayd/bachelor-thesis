
export type Answer = {
    Id?: number
    answer110: number
    answer: string
    questionId?: number
    userId: number | undefined
    id?: number
}