export type News = {
    Id?: number
    tweetId: string
    type: string
    city: string
    region: string
    id?: number
}