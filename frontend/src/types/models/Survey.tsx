export type Survey = {
    Id?: number
    newsId?: number
    authorId?: number
    questionIds: number[]
    ended: boolean
    id?: number
}