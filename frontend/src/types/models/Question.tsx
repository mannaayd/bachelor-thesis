export type Question = {
    Id?: number,
    description: string,
    surveyId?: number,
    respondentIds: number[],
    answerIds: number[],
    id?: number
}