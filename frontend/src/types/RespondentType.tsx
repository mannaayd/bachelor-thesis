export default interface RespondentType {
    id?: any | null,
    username?: string | null,
    email?: string,
    password?: string,
    region?: string,
    city?: string,
    role?: string
}