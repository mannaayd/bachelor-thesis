import UserType from "../UserType";

export type BoardState = {
    showManagerBoard: boolean,
    showAdminBoard: boolean,
    currentUser: UserType | undefined
}