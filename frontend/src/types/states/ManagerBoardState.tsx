import {Tweets} from "../models/Tweet";
import {Survey} from "../models/Survey";

export type ManagerBoardState = {
    tweets: Tweets,
    surveys: Survey[]
}