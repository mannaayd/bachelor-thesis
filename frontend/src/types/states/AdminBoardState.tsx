import {User} from "../models/User";

export type AdminBoardState = {
    users: User[]
}