import {Question} from "../models/Question";
import {Survey} from "../models/Survey";
import {Answer} from "../models/Answer";

export type UserBoardState = {
    content: string,
    answeredQuestions: Question[] | null,
    unansweredQuestions: Question[] | null,
    surveys: Survey[] | null,
    answers: Answer[] | null
    userId: number | undefined
}