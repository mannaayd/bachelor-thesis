export type LoginState = {
    username: string,
    password: string,
    loading: boolean,
    message: string,
    redirecting: boolean
};