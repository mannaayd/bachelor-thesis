

export type RegisterState = {
    username: string,
    email: string,
    password: string,
    region: string,
    city: string,
    successful: boolean,
    message: string
};