import UserType from "../UserType";

export type ProfileState = {
    redirect: string | null,
    userReady: boolean,
    currentUser: UserType | null
}