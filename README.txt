***********************************************************************************

                    News verification app   

***********************************************************************************

************************Requirements***********************************************

    Command line: bash, windows CLI, PowerShell, ...

    Docker: latest
    Docker-compose: latest

***********************************************************************************

**********************Installation guide*******************************************

    1. cl: docker-compose build
    2. cl: docker-compose up
    NOTE: Frontend: is running on port 3000
    NOTE: Backend: is running on port 8080
    NOTE: Nginx configured as reverse proxy
    3. Open browser
    4. Type to path http://localhost an then you can see frontend side
    NOTE: You can make HTTP requests directly to backend using path http://localhost/api
    5. Then you need to login into app
    NOTE: Admin login:admin password:12345678
    6. Now you can make surveys on tweets for verify news.
    7. If you want stop applcation you need cl: docker-compose down
   
************************************************************************************